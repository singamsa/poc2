//app.js

/*eslint-env node*/

//------------------------------------------------------------------------------
// node.js starter application for Bluemix
//------------------------------------------------------------------------------

// This application uses express as its web server
// for more info, see: http://expressjs.com
var express = require('express');

// cfenv provides access to your Cloud Foundry environment
// for more info, see: https://www.npmjs.com/package/cfenv
var cfenv = require('cfenv');

// create a new express server
var app = express();

// serve the files out of ./public as our main files
app.use(express.static(__dirname + '/public'));

// get the app environment from Cloud Foundry
var appEnv = cfenv.getAppEnv();

//code from the work space
var bodyParser = require('body-parser');
var fs = require("fs");

var mail;
var pwd;
var data;
var status = -1;
// Create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });


// start server on the specified port and binding host
app.listen(appEnv.port, '0.0.0.0', function() {
  // print a message when the server starts listening
  console.log("server starting on " + appEnv.url);
});




app.get('/index.html', function (req, res) {
   res.sendFile( __dirname + "/public/" + "index.html" );
})

app.post('/process_post', urlencodedParser, function (req, res) {
   // Prepare output in JSON format
   mail = req.body.mail_id;
   pwd = req.body.password;
   console.log(mail);
   console.log(pwd);
   updatePassword(res);
   
      if(status == 0){
		  status = -1;
		  res.writeHead(200, {"Content-Type": "text/html"});
			//res.end(JSON.stringify("SUCCESS"));
			res.end(JSON.stringify(mail));
		}
   else {
	   //status = -1;
	   res.writeHead(404, {"Content-Type": "text/plain"});
	   res.end("404 File Not Found");
   }
})

var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port
   
   console.log("Example app listening at http://%s:%s", host, port);

})

function updatePassword(res) {
 	
	data = require("./public/Users.json");
	data.forEach(function(elm){
		if(elm.mailId == mail) {
			elm.pwd = pwd;
			console.log(elm.mailId);
			console.log(elm.pwd);
			console.log("updating DB");
			status = 0;
			updateDB(res);
			console.log("updated DB");
		}
		
	});
	
}

function updateDB(res) {
	    var fs = require('fs');
        
		fs.writeFile("./public/Users.json", JSON.stringify(data), 'utf8', function (err){
			if(err) {
				console.log(err);
			}
			else {
				console.log("updated successfully2");
				status = 0;
			}
			console.log(JSON.stringify(data));
			fs.readFile("./public/Users.json", function (err, data1) {
      		if (err) {
         				return console.error(err);
      		}
      		console.log("Asynchronous read: " + data1.toString());
   });
		});
}

